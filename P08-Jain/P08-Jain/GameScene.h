//
//  GameScene.h
//  P08-Jain
//

//  Copyright (c) 2016 Astha Jain. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene<SKPhysicsContactDelegate>

@end
