//
//  GameOverScene.m
//  P08-Jain
//
//  Created by Astha Jain on 4/21/16.
//  Copyright © 2016 Astha Jain. All rights reserved.
//

#import "GameOverScene.h"
#import "GameScene.h"
@implementation GameOverScene

SKSpriteNode *paddleleft;
SKSpriteNode *paddleright;
NSInteger yourScore;
NSInteger highSc;
SKLabelNode *highScoreLb;
SKLabelNode *scoreLabel;

-(id)initWithSize:(CGSize)size {
    
    self = [super initWithSize:size];
    if(self) {
        
        
        SKAction *gameOver = [SKAction playSoundFileNamed:@"game_over" waitForCompletion:YES];
        [self runAction:gameOver];
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"back_ground_final_final.jpg"];
        //background.yScale = 3;
        background.position=CGPointMake(self.frame.size.width/2,self.frame.size.height/2);
        [self addChild:background];

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        highSc = [defaults integerForKey:@"HighScore"];
        yourScore = [defaults integerForKey:@"currentScore"];
        
        scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        scoreLabel.text = [NSString stringWithFormat:@"Score: %d", (int)yourScore];
        
        scoreLabel.fontSize = 75;
        scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - 300);
        [self addChild:scoreLabel];
        
        highScoreLb = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        highScoreLb.text = [NSString stringWithFormat:@"Best Score: %d", (int)highSc];
        
        highScoreLb.fontSize = 75;
        highScoreLb.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - 400);
        [self addChild:highScoreLb];
        
        paddleleft = [SKSpriteNode alloc];
        paddleleft = [SKSpriteNode spriteNodeWithImageNamed:@"paddleOne.png"];
        paddleleft.position = CGPointMake(CGRectGetMidX(self.frame)-270, 450);
        [self addChild:paddleleft];
        paddleleft.xScale = 1.0;
        paddleleft.yScale = 1.6;
        paddleleft.zRotation = -M_PI/2;
        
        paddleright = [[SKSpriteNode alloc] initWithImageNamed: @"paddleOne.png"];
        paddleright.position = CGPointMake(CGRectGetMidX(self.frame)+270, 450);
        [self addChild:paddleright];
        paddleright.xScale = 1.0;
        paddleright.yScale = 1.6;
        
        
        paddleright.zRotation = M_PI/2;
        
        SKSpriteNode* button;
        button = [[SKSpriteNode alloc] initWithImageNamed:@"playAgain.png"];
        button.name = @"Replay";
        button.xScale=1.5;
        button.yScale =1.2;
        button.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 100);
        [self addChild:button];
        
        
    }
    return self;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch* touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    
    SKNode *touchBody = [self nodeAtPoint:touchLocation];
    
    if(touchBody && [touchBody.name isEqualToString:@"Replay"]){
    
        GameScene* gameScene = [[GameScene alloc] initWithSize:self.size];
        [self.view presentScene:gameScene];
        NSLog(@"gameover");
    }
    
}

@end
