//
//  main.m
//  P08-Jain
//
//  Created by Astha Jain on 4/15/16.
//  Copyright © 2016 Astha Jain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
