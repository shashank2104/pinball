//
//  AppDelegate.h
//  P08-Jain
//
//  Created by Astha Jain on 4/15/16.
//  Copyright © 2016 Astha Jain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

