//
//  GameScene.m
//  P08-Jain
//
//  Created by Astha Jain on 4/15/16.
//  Copyright (c) 2016 Astha Jain. All rights reserved.
//

#import "GameScene.h"
#import "GameOverScene.h"
#import <AVFoundation/AVFoundation.h>

static NSString* ballCategoryName = @"ball";
static NSString* paddleLeftCategoryName = @"paddleLeft";
static NSString* paddleRightCategoryName = @"paddleRight";
static NSString* bottomCategoryName = @"bottom";
static NSString* bumperCategoryName = @"bumper";
static NSString* edgeCategoryName = @"edge";
static NSString *springCategoryName = @"spring";
static NSString *catcherCatergoryName = @"catcher";
static NSString *touchLeftName = @"left";
static NSString *touchRightName = @"right";

BOOL firstPlunge ;
BOOL paddleLeftTouched = FALSE;
BOOL paddleRightTouched = FALSE;

BOOL star1Lit = NO;
BOOL star2Lit = NO;
BOOL star3Lit = NO;

int lives = 3;
int contactCounter = 0;
int score = 0;
int highScore = 0;

static const uint32_t ballCategory  = 1 << 0;
static const uint32_t bottomCategory = 1 << 1;
static const uint32_t paddleLeftCategory = 1 << 2;
static const uint32_t paddleRightCategory = 1 << 3;
static const uint32_t edgeCategory = 1 << 4;
static const uint32_t star1Category = 1 << 5;
static const uint32_t star2Category = 1 << 6;
static const uint32_t star3Category = 1 << 7;
static const uint32_t springCategory = 1 << 8;
static const uint32_t catcherCategory = 1 << 9;
static const uint32_t bumper100Category = 1 << 10;
static const uint32_t bumper200Category = 1 << 11;
static const uint32_t bumper300Category = 1 << 12;
static const uint32_t frogCategory = 1 << 13;

SKSpriteNode *background;
SKSpriteNode *ball;
SKSpriteNode *paddleLeft;
SKSpriteNode *paddleRight;
SKSpriteNode *bumper100;
SKSpriteNode *bumper200;
SKSpriteNode *bumper300;
SKSpriteNode *spring;
SKSpriteNode *catcher;
SKSpriteNode *candy;
SKSpriteNode *touchLeft;
SKSpriteNode *touchRight;
SKSpriteNode *star1, *star2, *star3;
SKSpriteNode *frog;
SKLabelNode *label;
SKLabelNode *livesLB;
SKLabelNode *scoreLb;
SKLabelNode *fadingScore;
AVAudioPlayer *player;
SKAction* playBackgroundMusic;
NSUserDefaults *defaults;

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    
    /* Setup your scene here*/
    //get your prev highscore here
    defaults = [NSUserDefaults standardUserDefaults];
    highScore = (int)[defaults integerForKey:@"HighScore"];
    //playBackgroundMusic = [SKAction repeatActionForever:[SKAction playSoundFileNamed:@"background_music.mp3" waitForCompletion:YES]];
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"background_music"
                                         ofType:@"mp3"]];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.numberOfLoops = -1;
    
    //score = 0
    score = 0;
    
 
    //Adding gravity to the scene
    self.physicsWorld.gravity = CGVectorMake(0.0f, -9.0f);
    
    SKPhysicsBody *borderBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody = borderBody;
    self.physicsBody.friction = 0.0f;
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"back_ground_final_final.jpg"];
   // background.yScale = 0.78;
    background.position=CGPointMake(self.frame.size.width/2,self.frame.size.height/2);
    [self addChild:background];
    
    lives=3;
    // NSLog(@"%f..%f", self.frame.size.width, self.frame.size.height);
    
    ball = [SKSpriteNode spriteNodeWithImageNamed:@"Candy.png"];
    ball.position=CGPointMake(2*CGRectGetMidX(self.frame)-45, 320);
    ball.name = ballCategoryName;
    SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
    [ball runAction:[SKAction repeatActionForever:action]];
    ball.xScale = 2;
    ball.yScale = 2;
    [self addChild:ball];
    ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:ball.frame.size.width/2];
    ball.physicsBody.friction = 0.0f;
    ball.physicsBody.restitution = 0.8f;
    ball.physicsBody.linearDamping = 0.6f;
    ball.physicsBody.allowsRotation = NO;
    ball.physicsBody.categoryBitMask = ballCategory;
//    ball.physicsBody.collisionBitMask = 1 << 0;
    ball.physicsBody.contactTestBitMask = paddleRightCategory | paddleLeftCategory | bottomCategory | bumper100Category | bumper200Category | bumper300Category | springCategory | catcherCategory | star1Category | star2Category | star3Category | frogCategory;
    ball.physicsBody.dynamic = NO;
    //[ball.physicsBody applyImpulse:CGVectorMake(50.0f, 1800.0f)];
    
    /* Not needed as of now
     label = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
     label.text = @"Pinball";
     label.fontSize = 80;
     label.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
     [self addChild:label];
     */
    
    star1 = [SKSpriteNode alloc];
    star1 = [SKSpriteNode spriteNodeWithImageNamed:@"star1.png"];
    star1.name = @"star1";
    star1.position = CGPointMake(CGRectGetMidX(self.frame)-270, CGRectGetMaxY(self.frame) - 180.0);
    [self addChild:star1];
    star1.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:star1.size.width/2];
    star1.physicsBody.restitution = 0.1f;
    star1.physicsBody.friction = 0.4f;
    star1.physicsBody.dynamic = NO;
    star1.physicsBody.categoryBitMask = star1Category;
    star1.physicsBody.collisionBitMask = 1<<0;
    
    star2 = [SKSpriteNode alloc];
    star2 = [SKSpriteNode spriteNodeWithImageNamed:@"star1.png"];
    star2.name = @"star2";
    star2.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - 140.0);
    [self addChild:star2];
    star2.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:star2.size.width/2];
    star2.physicsBody.restitution = 0.0f;
    star2.physicsBody.friction = 0.0f;
    star2.physicsBody.dynamic = NO;
    star2.physicsBody.categoryBitMask = star2Category;
    star2.physicsBody.collisionBitMask = 1<<0;
    
    star3 = [SKSpriteNode alloc];
    star3 = [SKSpriteNode spriteNodeWithImageNamed:@"star1.png"];
    star3.name = @"star3";
    star3.position = CGPointMake(CGRectGetMidX(self.frame)+270, CGRectGetMaxY(self.frame) - 180.0);
    [self addChild:star3];
    star3.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:star3.size.width/2];
    star3.physicsBody.restitution = 0.0f;
    star3.physicsBody.friction = 0.0f;
    star3.physicsBody.dynamic = NO;
    star3.physicsBody.categoryBitMask = star3Category;
    star3.physicsBody.collisionBitMask = 1<<0;
    
    //lives label on top middle
    livesLB = [SKLabelNode labelNodeWithFontNamed:@"System"];
    livesLB.text = [NSString stringWithFormat:@"%d", lives];
    
    livesLB.fontSize = 50;
    livesLB.position = CGPointMake(CGRectGetWidth(self.frame)-80, CGRectGetMaxY(self.frame) - 90);
    [self addChild:livesLB];
    
    scoreLb = [SKLabelNode labelNodeWithFontNamed:@"System"];
    scoreLb.text = [NSString stringWithFormat:@"%d", score];
    
    scoreLb.fontSize = 50;
    scoreLb.position = CGPointMake(90, CGRectGetMaxY(self.frame) - 90);
    [self addChild:scoreLb];

    fadingScore = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    fadingScore.text = [NSString stringWithFormat:@"%d", score];
    fadingScore.fontSize = 75;
    fadingScore.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+250);
    [self addChild:fadingScore];
    fadingScore.alpha = 0;

    paddleLeft = [SKSpriteNode alloc];
    paddleLeft = [SKSpriteNode spriteNodeWithImageNamed:@"paddleOne.png"];
    paddleLeft.name = paddleLeftCategoryName;
    paddleLeft.position = CGPointMake(CGRectGetMidX(self.frame)-200, 250);
    [self addChild:paddleLeft];
    paddleLeft.xScale = 1.1;
    paddleLeft.yScale = 1.6;
    paddleLeft.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paddleLeft.frame.size];
    paddleLeft.physicsBody.restitution = 0.1f;
    paddleLeft.physicsBody.friction = 0.4f;
    //CGPoint anchorLeft = CGPointMake(0,0);
    paddleLeft.zRotation = -0.5;
    //[paddleLeft setAnchorPoint:anchorLeft];
    // make physicsBody static
    paddleLeft.physicsBody.dynamic = NO;
    paddleLeft.physicsBody.categoryBitMask = paddleLeftCategory;
    
    paddleRight = [[SKSpriteNode alloc] initWithImageNamed: @"paddleOne.png"];
    paddleRight.position = CGPointMake(CGRectGetMidX(self.frame)+200, 250);
    paddleRight.name = paddleRightCategoryName;
    [self addChild:paddleRight];
    paddleRight.xScale = 1.1;
    paddleRight.yScale = 1.6;
    paddleRight.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:paddleRight.frame.size];
    paddleRight.zRotation = 0.5;
    paddleRight.physicsBody.restitution = 0.1f;
    paddleRight.physicsBody.friction = 0.4f;
    // make physicsBody static
    paddleRight.physicsBody.dynamic = NO;
    paddleRight.physicsBody.categoryBitMask = paddleRightCategory;
    //CGPoint anchorRight = CGPointMake(1,0.5);
    
    
    
    //[paddleRight setAnchorPoint:anchorRight];
    /*  background = (SKSpriteNode *)[self childNodeWithName:@"background"];
     ball = (SKSpriteNode *)[self childNodeWithName:@"ball"];
     ball.physicsBody.affectedByGravity = true;
     [ball.physicsBody applyImpulse:CGVectorMake(10.0f, -50.0f)];
     */
    
    //add plunger (spring)
    //UIImageView *spring = [[UIImageView alloc] initWithFrame:CGRectMake(2*CGRectGetMidX(self.frame)-45,300,80, 300)];
    //spring.image = [UIImage imageNamed:@"gm-spring.png"];
    spring = [[SKSpriteNode alloc] initWithImageNamed:@"gm-spring.png"];
    spring.position = CGPointMake(2*CGRectGetMidX(self.frame)-45,0);
    spring.name = springCategoryName;
    spring.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:spring.frame.size];
    spring.physicsBody.restitution = 0.1f;
    spring.physicsBody.friction = 0.4f;
    spring.physicsBody.dynamic = NO;
    spring.physicsBody.categoryBitMask = springCategory;
    CGPoint springAnchor = CGPointMake(0.5, 0);
    [spring setAnchorPoint:springAnchor];
    firstPlunge = YES;
    [self addChild:spring];
    
    //add a catcher on left side of screen
    catcher = [[SKSpriteNode alloc] init];
    catcher.position = CGPointMake(10,10);
    catcher.name = catcherCatergoryName;
    catcher.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:spring.frame.size];
    catcher.yScale = 0.5;
    catcher.physicsBody.dynamic = NO;
    catcher.physicsBody.categoryBitMask = catcherCategory;
    [self addChild:catcher];
    
    SKTexture* frogTexture1 = [SKTexture textureWithImageNamed:@"original"];
    frogTexture1.filteringMode = SKTextureFilteringNearest;
    SKTexture* frogTexture2 = [SKTexture textureWithImageNamed:@"OmNom"];
    frogTexture2.filteringMode = SKTextureFilteringNearest;
    
    frog = [[SKSpriteNode alloc] initWithImageNamed:@"original.png"];
    SKAction* flip = [SKAction repeatActionForever:[SKAction animateWithTextures:@[frogTexture1, frogTexture2] timePerFrame:0.6]];
    [frog runAction:flip];
    frog.position = CGPointMake(230.0, CGRectGetMidY(self.frame) - 170.0);
    frog.name = catcherCatergoryName;
    frog.xScale = 0.6;
    frog.yScale = 0.6;
    frog.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:frog.frame.size];
    frog.physicsBody.dynamic = NO;
    frog.physicsBody.categoryBitMask = frogCategory;
    [self addChild:frog];
    SKAction* moveRight = [SKAction moveToX:(CGRectGetMaxX(self.frame)-230) duration:2.0];
    SKAction* moveLeft = [SKAction moveToX:(230) duration:2.0];
    SKAction* moveSeq = [SKAction sequence:@[moveRight, moveLeft]];
    SKAction* moveForever = [SKAction repeatActionForever:moveSeq];
    [frog runAction:moveForever];

    //bottom
    CGRect bottom = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width -100 , 1);
    SKNode * bottomNode=[SKNode node];
    bottomNode.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:bottom];
    bottomNode.physicsBody.categoryBitMask = bottomCategory;
    [self addChild:bottomNode];
  
    /*UIBezierPath *rightUpperPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.frame) + 190, CGRectGetMidY(self.frame) + 550.0)
                                                                  radius:100
                                                              startAngle:0
                                                                endAngle:90 * M_PI / 180
                                                               clockwise:YES];*/
    
  UIBezierPath *rightUpperPath = [UIBezierPath bezierPath];
    [rightUpperPath moveToPoint:CGPointMake(self.frame.size.width, CGRectGetMidY(self.frame) + 750.0)];
    [rightUpperPath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame)+190, CGRectGetHeight(self.frame))];
    [rightUpperPath closePath];

    SKNode* rightUpperCorner = [SKNode node];
    rightUpperCorner.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:rightUpperPath.CGPath];
    rightUpperCorner.physicsBody.restitution = 0.0f;
    rightUpperCorner.physicsBody.friction = 0.0f;
    rightUpperCorner.physicsBody.dynamic = NO;
    rightUpperCorner.name = edgeCategoryName;
    [self addChild:rightUpperCorner];
    rightUpperCorner.physicsBody.categoryBitMask = edgeCategory;
    
   /* UIBezierPath *leftUpperPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.frame) -135, 770)
                                                                 radius:250
                                                             startAngle:90 * M_PI / 180
                                                               endAngle:199 * M_PI / 180
                                                              clockwise:YES];*/

    //NSLog(@"%f, %f", (CGRectGetHeight(self.frame)-(CGRectGetMidY(self.frame) + 750.0)), CGRectGetWidth(self.frame)-(CGRectGetMidX(self.frame)+190));
    UIBezierPath *leftUpperPath = [UIBezierPath bezierPath];
    [leftUpperPath moveToPoint:CGPointMake(0, CGRectGetMidY(self.frame) + 750.0)];
    [leftUpperPath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame)-190, CGRectGetHeight(self.frame))];
    [leftUpperPath closePath];

    SKNode* leftUpperCorner = [SKNode node];
    leftUpperCorner.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:leftUpperPath.CGPath];
    leftUpperCorner.physicsBody.restitution = 0.0f;
    leftUpperCorner.physicsBody.friction = 0.0f;
    leftUpperCorner.physicsBody.dynamic = NO;
    leftUpperCorner.name = edgeCategoryName;
    [self addChild:leftUpperCorner];
    leftUpperCorner.physicsBody.categoryBitMask = edgeCategory;
  
    UIBezierPath *leftLaneBorderPath = [UIBezierPath bezierPath];
    [leftLaneBorderPath moveToPoint:CGPointMake(100, 0.0)];
    [leftLaneBorderPath addLineToPoint:CGPointMake(100.0, CGRectGetMidY(self.frame))];
    [leftLaneBorderPath closePath];
    
    SKNode* leftLaneBorder = [SKNode node];
    leftLaneBorder.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:leftLaneBorderPath.CGPath];
    leftLaneBorder.physicsBody.restitution = 0.0f;
    leftLaneBorder.physicsBody.friction = 0.0f;
    leftLaneBorder.physicsBody.dynamic = NO;
    leftLaneBorder.name = edgeCategoryName;
    [self addChild:leftLaneBorder];
    
    UIBezierPath *rightLaneBorderPath = [UIBezierPath bezierPath];
    [rightLaneBorderPath moveToPoint:CGPointMake(self.frame.size.width - 90, 250.0)];
    [rightLaneBorderPath addLineToPoint:CGPointMake(CGRectGetWidth(self.frame)-90.0, CGRectGetMidY(self.frame))];
    [rightLaneBorderPath closePath];
    
    SKNode* rightLaneBorder = [SKNode node];
    rightLaneBorder.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:rightLaneBorderPath.CGPath];
    rightLaneBorder.physicsBody.restitution = 0.0f;
    rightLaneBorder.physicsBody.friction = 0.0f;
    rightLaneBorder.physicsBody.dynamic = NO;
    rightLaneBorder.name = edgeCategoryName;
    [self addChild:rightLaneBorder];
    
    UIBezierPath *leftPath = [UIBezierPath bezierPath];
    [leftPath moveToPoint:CGPointMake(90.0, CGRectGetMidY(self.frame) - 200.0)];
    [leftPath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame)-190, 250)];
    
    [leftPath closePath];
    
    SKNode* leftPathBorder = [SKNode node];
    leftPathBorder.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:leftPath.CGPath];
    leftPathBorder.physicsBody.restitution = 0.0f;
    leftPathBorder.physicsBody.friction = 0.0f;
    leftPathBorder.physicsBody.dynamic = NO;
    leftPathBorder.name = edgeCategoryName;
    [self addChild:leftPathBorder];
    
    
    UIBezierPath *rightPath = [UIBezierPath bezierPath];
    [rightPath moveToPoint:CGPointMake(self.frame.size.width -95.0 , CGRectGetMidY(self.frame) - 200.0)];
    [rightPath addLineToPoint:CGPointMake(CGRectGetMidX(self.frame)+230, 240)];
    
    [rightPath closePath];
    
    SKNode* rightPathBorder = [SKNode node];
    rightPathBorder.physicsBody = [SKPhysicsBody bodyWithEdgeChainFromPath:rightPath.CGPath];
    rightPathBorder.physicsBody.restitution = 0.0f;
    rightPathBorder.physicsBody.friction = 0.0f;
    rightPathBorder.physicsBody.dynamic = NO;
    rightPathBorder.name = edgeCategoryName;
    [self addChild:rightPathBorder];
    
    
    
    
    bumper100 = [SKSpriteNode alloc];
    bumper100 = [SKSpriteNode spriteNodeWithImageNamed:@"bumper_100.png"];
    bumper100.name = bumperCategoryName;
    bumper100.position = CGPointMake(CGRectGetMidX(self.frame)-270, CGRectGetMidY(self.frame) + 380.0);
    [self addChild:bumper100];
    bumper100.xScale = 0.3;
    bumper100.yScale = 0.3;
    bumper100.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:bumper100.size.width/2];
    bumper100.physicsBody.restitution = 0.1f;
    bumper100.physicsBody.friction = 0.4f;
    bumper100.physicsBody.dynamic = NO;
    bumper100.physicsBody.categoryBitMask = bumper100Category;

    bumper200 = [SKSpriteNode alloc];
    bumper200 = [SKSpriteNode spriteNodeWithImageNamed:@"bumper_200.png"];
    bumper200.name = bumperCategoryName;
    bumper200.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 160.0);
    [self addChild:bumper200];
    bumper200.xScale = 0.3;
    bumper200.yScale = 0.3;
    bumper200.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:bumper200.size.width/2];
    bumper200.physicsBody.restitution = 0.1f;
    bumper200.physicsBody.friction = 0.4f;
    bumper200.physicsBody.dynamic = NO;
    bumper200.physicsBody.categoryBitMask = bumper200Category;

    bumper300 = [SKSpriteNode alloc];
    bumper300 = [SKSpriteNode spriteNodeWithImageNamed:@"bumper_300.png"];
    bumper300.name = bumperCategoryName;
    bumper300.position = CGPointMake(CGRectGetMidX(self.frame)+270, CGRectGetMidY(self.frame) + 370.0);
    [self addChild:bumper300];
    bumper300.xScale = 0.3;
    bumper300.yScale = 0.3;
    bumper300.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:bumper300.size.width/2];
    bumper300.physicsBody.restitution = 0.1f;
    bumper300.physicsBody.friction = 0.4f;
    bumper300.physicsBody.dynamic = NO;
    bumper300.physicsBody.categoryBitMask = bumper300Category;

    
    //adding touch location to the screen
    CGSize touchSize = CGSizeMake(460, 460);
    //left touch zone
    touchLeft = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:touchSize];
    touchLeft.position = CGPointMake(CGRectGetMidX(self.frame)-230, 230);
    touchLeft.name = touchLeftName;
    [self addChild:touchLeft];
    //right touch zone
    touchRight = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:touchSize];
    touchRight.position = CGPointMake(CGRectGetMidX(self.frame)+230, 230);
    touchRight.name = touchRightName;
    [self addChild:touchRight];
    
    
    //play music in background forever until game over
    //[self runAction:playBackgroundMusic];
    [player play];
    
    //commented becuase works anyways
    //Set delegate
    self.physicsWorld.contactDelegate = self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    UITouch* touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    
    SKNode *touchBody = [self nodeAtPoint:touchLocation];
    
    if([touchBody.name isEqualToString:springCategoryName] && firstPlunge==YES){
        
        SKAction *squeeze = [SKAction scaleYTo:0.3 duration:0];
        SKAction *relax = [SKAction scaleYTo:1.0 duration:0.1];
        SKAction *sound = [SKAction playSoundFileNamed:@"spring.wav" waitForCompletion:nil];
        SKAction *springSeq = [SKAction sequence:@[squeeze, relax, sound]];
        [spring runAction:springSeq];
        ball.physicsBody.dynamic = YES;
        
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 800.0f)];
        firstPlunge = NO;
        contactCounter = 0;
    }
    
    //animate spring anyway when touched
    else if([touchBody.name isEqualToString:springCategoryName]){
        SKAction *squeeze = [SKAction scaleYTo:0.3 duration:0];
        SKAction *relax = [SKAction scaleYTo:1.0 duration:0.1];
        SKAction *sound = [SKAction playSoundFileNamed:@"spring.wav" waitForCompletion:nil];
        SKAction *springSeq = [SKAction sequence:@[squeeze, relax, sound]];
        [spring runAction:springSeq];
    }
    
    
    
    //    if(touchLocation.x<=self.frame.size.width/2 && touchLocation.y <=self.frame.size.height/2){
    if([touchBody.name isEqualToString:touchLeftName]){
        paddleLeftTouched=TRUE;
        SKAction *rotate = [SKAction rotateByAngle:M_PI/2.5 duration:0.1];
        SKAction *playSound = [SKAction playSoundFileNamed:@"FlipperUp2.wav" waitForCompletion:nil];
        SKAction *rotateDown = [SKAction rotateByAngle:-(M_PI/2.5) duration:0.1];
        SKAction *seq = [SKAction sequence:@[rotate,playSound, rotateDown]];
        [paddleLeft runAction:seq];
    }
    else if([touchBody.name isEqualToString:touchRightName]){
        paddleRightTouched=TRUE;
        SKAction *rotate = [SKAction rotateByAngle:-(M_PI/2.5) duration:0.1];
        SKAction *playSound = [SKAction playSoundFileNamed:@"FlipperUp2.wav" waitForCompletion:nil];
        SKAction *rotateDown = [SKAction rotateByAngle:M_PI/2.5 duration:0.1];
        SKAction *seq = [SKAction sequence:@[rotate, playSound, rotateDown]];
        [paddleRight runAction:seq];
    }
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    /*  UITouch* touch = [touches anyObject];
     CGPoint touchLocation = [touch locationInNode:self];
     
     SKNode *touchBody = [self nodeAtPoint:touchLocation];
     if([touchBody.name isEqualToString:paddleLeftCategoryName]){
     SKAction *rotateDown = [SKAction rotateToAngle:+45 * M_PI/180 duration:0.1];
     [paddleLeft runAction:rotateDown];
     }
     else if([touchBody.name isEqualToString:paddleRightCategoryName]){
     SKAction *rotateDown = [SKAction rotateToAngle:+45 * M_PI/180 duration:0.1];
     [paddleRight runAction:rotateDown];
     }*/
    paddleLeftTouched=FALSE;
    paddleRightTouched=FALSE;
    
    
}

- (void)didBeginContact:(SKPhysicsContact*)contact {
    
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    //Determine contact
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == paddleLeftCategory && paddleLeftTouched) {
    //    NSLog(@"Left paddle hit");
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 600.0f)];
    }

    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == springCategory) {
       // NSLog(@"Spring hit");
        SKAction *squeeze = [SKAction scaleYTo:0.3 duration:0];
        SKAction *relax = [SKAction scaleYTo:1.0 duration:0.1];
        SKAction *sound = [SKAction playSoundFileNamed:@"spring.wav" waitForCompletion:nil];
        SKAction *springSeq = [SKAction sequence:@[squeeze, relax, sound]];
        [spring runAction:springSeq];
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 800.0f)];
    }

    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == paddleRightCategory && paddleRightTouched) {
    //    NSLog(@"Right paddle hit");
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 600.0f)];
        [ball.physicsBody applyAngularImpulse:150];
    }
    
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == bumper100Category) {
        SKAction *squeezeY = [SKAction scaleYTo:0.25 duration:0];
        SKAction *squeezeX = [SKAction scaleXTo:0.25 duration:0];
        SKAction *relaxX = [SKAction scaleXTo:0.3 duration:0.1];
        SKAction *relaxY = [SKAction scaleYTo:0.3 duration:0.1];
        SKAction *playSound = [SKAction playSoundFileNamed:@"bump.wav" waitForCompletion:nil];
        SKAction *bumperSeq = [SKAction sequence:@[squeezeX, squeezeY, relaxX, relaxY, playSound]];
        [bumper100 runAction:bumperSeq];
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 300.0f)];
        score = score + 100;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
      //  NSLog(@"100 hit");
        [self fadeScore:(100)];

    }
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == bumper200Category) {
        SKAction *squeezeY = [SKAction scaleYTo:0.25 duration:0];
        SKAction *squeezeX = [SKAction scaleXTo:0.25 duration:0];
        SKAction *relaxX = [SKAction scaleXTo:0.3 duration:0.1];
        SKAction *relaxY = [SKAction scaleYTo:0.3 duration:0.1];
        SKAction *playSound = [SKAction playSoundFileNamed:@"bump.wav" waitForCompletion:nil];
        SKAction *bumperSeq = [SKAction sequence:@[squeezeX, squeezeY, relaxX, relaxY, playSound]];
        [bumper200 runAction:bumperSeq];
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 300.0f)];
        score = score + 200;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
     //   NSLog(@"200 hit");
        [self fadeScore:(200)];

    }
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == bumper300Category) {
        SKAction *squeezeY = [SKAction scaleYTo:0.25 duration:0];
        SKAction *squeezeX = [SKAction scaleXTo:0.25 duration:0];
        SKAction *relaxX = [SKAction scaleXTo:0.3 duration:0.1];
        SKAction *relaxY = [SKAction scaleYTo:0.3 duration:0.1];
        SKAction *playSound = [SKAction playSoundFileNamed:@"bump.wav" waitForCompletion:nil];
        SKAction *bumperSeq = [SKAction sequence:@[squeezeX, squeezeY, relaxX, relaxY, playSound]];
        [bumper300 runAction:bumperSeq];
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 300.0f)];
        score = score + 300;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
     //   NSLog(@"300 hit");
        [self fadeScore:(300)];

    }

    SKTexture* starTexture1 = [SKTexture textureWithImageNamed:@"star1"];
    starTexture1.filteringMode = SKTextureFilteringNearest;
    SKTexture* starTexture2 = [SKTexture textureWithImageNamed:@"star2"];
    starTexture2.filteringMode = SKTextureFilteringNearest;

    // Star
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == star1Category) {
        SKAction* flip = [SKAction repeatAction:[SKAction animateWithTextures:@[starTexture2, starTexture1] timePerFrame:0.2] count:1];
        [star1 runAction:flip];
        
        score = score + 50;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
        [self fadeScore:(50)];

    }
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == star2Category) {
        SKAction* flip = [SKAction repeatAction:[SKAction animateWithTextures:@[starTexture2, starTexture1] timePerFrame:0.2] count:1];
        [star2 runAction:flip];
        
        score = score + 50;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
        [self fadeScore:(50)];

    }
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == star3Category) {
        SKAction* flip = [SKAction repeatAction:[SKAction animateWithTextures:@[starTexture2, starTexture1] timePerFrame:0.2] count:1];
        [star3 runAction:flip];
        
        score = score + 50;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
        
        [self fadeScore:(50)];
    }

    //catcher category collision
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask ==catcherCategory){
        
        //SKAction *playSound = []
        SKAction *fall = [SKAction playSoundFileNamed:@"fall.wav" waitForCompletion:YES];
        SKAction *shoot = [SKAction playSoundFileNamed:@"shoot.wav" waitForCompletion:nil];
        [catcher runAction:fall];
        ball.physicsBody.dynamic = NO;
        [NSThread sleepForTimeInterval:2];
        ball.physicsBody.dynamic = YES;
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 1000.0f)];
        [catcher runAction:shoot];
        score = score + 500;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
        
        [self fadeScore:(500)];
    }
    
    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask ==frogCategory){
        
        //SKAction *playSound = []
      //  SKAction *fall = [SKAction playSoundFileNamed:@"fall.wav" waitForCompletion:YES];
       // SKAction *shoot = [SKAction playSoundFileNamed:@"shoot.wav" waitForCompletion:nil];
       // [catcher runAction:fall];
        [frog removeAllActions];
      //  ball.physicsBody.dynamic = NO;
    //    [NSThread sleepForTimeInterval:0.4];
     //   ball.physicsBody.dynamic = YES;
        [ball.physicsBody applyImpulse:CGVectorMake(5.0f, 600.0f)];
      //  [catcher runAction:shoot];
        score = score - 100;
        if(score>=highScore){
            highScore = score;
        }
        scoreLb.text = [NSString stringWithFormat:@"%d", score];
        
        SKTexture* frogTexture1 = [SKTexture textureWithImageNamed:@"original"];
        frogTexture1.filteringMode = SKTextureFilteringNearest;
        SKTexture* frogTexture2 = [SKTexture textureWithImageNamed:@"OmNom"];
        frogTexture2.filteringMode = SKTextureFilteringNearest;
        SKAction* flip = [SKAction repeatActionForever:[SKAction animateWithTextures:@[frogTexture1, frogTexture2] timePerFrame:0.6]];
        [frog runAction:flip];
        SKAction* moveRight = [SKAction moveToX:(CGRectGetMaxX(self.frame)-230) duration:2.0];
        SKAction* moveLeft = [SKAction moveToX:(230) duration:2.0];
        SKAction* moveSeq = [SKAction sequence:@[moveRight, moveLeft]];
        SKAction* moveForever = [SKAction repeatActionForever:moveSeq];
        [frog runAction:moveForever];

        [self fadeScore:(-100)];
    }

    if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == bottomCategory && contactCounter==0){
      //  NSLog(@"bottom hit");
        lives--;
        contactCounter++;
        ball.physicsBody.dynamic = NO;
        if(lives>0){
            [self resetBall];
        }
        else{
            //add your gameover output (scene or alert)
            //[self removeActionForKey:@"playBackgroundMusic"];
            [player stop];
            
            [defaults setInteger:highScore forKey:@"HighScore"];
            [defaults setInteger:score forKey:@"currentScore"];
            GameOverScene *gameover = [[GameOverScene alloc] initWithSize:self.frame.size];
            [self.view presentScene:gameover];
           
        }
    }
    
    
}

-(void)resetBall{
    //resets the ball back to spring position decrementing a life
    SKPhysicsBody *newball = ball.physicsBody;
    ball.physicsBody = nil;
    livesLB.text = [NSString stringWithFormat:@"%d", lives];
    ball.position=CGPointMake(2*CGRectGetMidX(self.frame)-45, 320);
    ball.physicsBody = newball;
    firstPlunge = YES;
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
  /*  if((CGRectContainsRect(ball.frame, star3.frame)) && star3Lit == NO){
        star3.alpha = 1.0;
        star3.texture = [SKTexture textureWithImageNamed:@"star2.png"];
        star3Lit = YES;
    }
    else {
        star3.alpha = 0.5;
        star3.texture = [SKTexture textureWithImageNamed:@"star1.png"];
        star3Lit = NO;
    }*/
    
}

-(void)fadeScore:(int)x{
    fadingScore.alpha = 1.0;
    if(x < 0) {
        fadingScore.text = [NSString stringWithFormat:@"%d", x];
    }else {
        fadingScore.text = [NSString stringWithFormat:@"+%d", x];
    }
    SKAction* fadeOut = [SKAction fadeAlphaTo:0 duration:0.25];
    SKAction* moveUp = [SKAction moveToY:(CGRectGetMidY(self.frame)+335) duration:0.5];
    SKAction* moveDown = [SKAction moveToY:(CGRectGetMidY(self.frame)+250) duration:0];
    SKAction* fadeSeq = [SKAction sequence:@[moveUp, fadeOut, moveDown]];
    [fadingScore runAction:fadeSeq];
}

@end
